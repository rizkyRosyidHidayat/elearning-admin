import Vue from 'vue'
import Vuex from 'vuex'
import firebase from '../config/firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    dataKategori: [],
    status: null
  },
  mutations: {
    updateLoading (state, payload) {
      state.loading = payload
    },
    updateStatus (state, payload) {
      state.status = payload
    },
    updateDataKategori (state, payload) {
      state.dataKategori = payload
    }
  },
  actions: {
    updateStatus (ctx, payload) {
      ctx.commit('updateStatus', payload)
    },
    updateLoading (ctx, payload) {
      ctx.commit('updateLoading', payload)
    },
    async getDataKategori (ctx) {
      ctx.dispatch('updateLoading', true)
      try {
        await firebase.firestore().collection('kategori').get()
          .then(snapshot => {
            let data = []
            snapshot.docs.map((doc, i) => {
              data[i] = {...doc.data(), id: doc.id}
            })
            ctx.commit('updateDataKategori', data)
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    },
    async postDataMateri (ctx, payload) {
      ctx.dispatch('updateLoading', true)
      try {
        await firebase.firestore().collection('materi').add(payload)
          .then(() => {
            ctx.dispatch('updateStatus', true)
          })
      } catch (e) {
        ctx.dispatch('updateStatus', false)
      } finally {
        setTimeout(() => ctx.dispatch('updateStatus', null), 2000)
        ctx.dispatch('updateLoading', false)
      }
    }
  },
  modules: {
  }
})
